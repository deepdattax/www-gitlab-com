---
layout: handbook-page-toc
title: Personal Data Access Requests
category: GitLab.com
subcategory: Legal
description: >-
  Support Engineering workflows for non-deletion requests relating to personal
  data
---

This page has been moved. Please see the [Personal Data Access and Account Deletion](/handbook/support/workflows/personal_data_access_account_deletion.html) page.
